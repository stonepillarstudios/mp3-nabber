package com.sps.mus;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.jorgecastilloprz.FABProgressCircle;
import com.github.jorgecastilloprz.listeners.FABProgressListener;
import com.sps.mus.utils.JanTools;
import com.sps.mus.utils.LiedjiesContract;
import com.sps.mus.utils.LiedjiesDBHelper;
import com.sps.mus.utils.LiedjiesHelper;
import com.sps.mus.utils.StatsHelper;

import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ShareReceiverActivity extends Activity {

    private StatsHelper statsHelper;
    private TextView txtProgress;
    private FABProgressCircle progressCircle;

    private Boolean outcome = null;
    private String outcomeText;
    private boolean quickCompletion = true;

    private LiedjiesHelper liedjiesHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share_receiver);

        statsHelper = new StatsHelper(this);
        liedjiesHelper = new LiedjiesHelper(this);

        txtProgress = findViewById(R.id.txtShare);
        progressCircle = findViewById(R.id.progShare);
        progressCircle.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View view, int i, int i1, int i2, int i3, int i4, int i5, int i6, int i7) {

                if (outcome != null){

                    ImageView img = view.findViewById(R.id.completeFabIcon);

                    if (outcome == false && img != null) {
                        img.setImageResource(R.drawable.ic_warning_white_36dp);
                    }

                }

            }
        });
        progressCircle.attachListener(new FABProgressListener() {
            @Override
            public void onFABProgressAnimationEnd() {
                txtProgress.setText(outcomeText);
                findViewById(R.id.linLayButtons).setVisibility(View.VISIBLE);
            }
        });

        ((TextView) findViewById(R.id.btnNabber)).setText(
                String.format(getString(R.string.open_app), getString(R.string.app_name))
        );

    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        if (hasFocus){
            progressCircle.show();
            handleIntent(getIntent());
        }
    }

    private void handleIntent(Intent intent){
        if (intent != null) { if (intent.getStringExtra(Intent.EXTRA_TEXT) != null){

            String stringExtra = intent.getStringExtra(Intent.EXTRA_TEXT);
            System.out.println("INTENT: " + stringExtra);

            boolean success = true;
            if (stringExtra.contains("https://shz.am") || stringExtra.contains("https://www.shazam.com")) {   //from shazam (or new shazam)
                stringExtra = stringExtra.substring(stringExtra.indexOf("discover ") + "discover ".length());
                String songTitle = stringExtra.substring(0, stringExtra.indexOf(" by "));
                String artist = stringExtra.substring(stringExtra.indexOf(" by ") + " by ".length(), stringExtra.indexOf("https://") - 2);

                quickCompletion = true;
                addSongToList(songTitle, artist);

            } else if (stringExtra.contains("https://g.co")) {  //from google
                String songTitle = stringExtra.substring(0, stringExtra.indexOf("https://g.co") - 1);
                String url = stringExtra.substring(stringExtra.indexOf("https://g.co"), stringExtra.length() - 1);

                quickCompletion = false;
                new GetGoogleInfo().execute(url, songTitle);

            } else if (stringExtra.contains("https://bnc.lt") || stringExtra.contains("http://soundhound.com")) { //from soundhound
                stringExtra = stringExtra.substring(stringExtra.indexOf("find ") + "find ".length());
                String songTitle = stringExtra.substring(0, stringExtra.indexOf(" by "));
                String artist;
                if (stringExtra.contains("https://")) {
                    artist = stringExtra.substring(stringExtra.indexOf(" by ") + " by ".length(), stringExtra.indexOf("https://") - 1);
                }else{
                    artist = stringExtra.substring(stringExtra.indexOf(" by ") + " by ".length(), stringExtra.indexOf("http://") - 1);
                }

                quickCompletion = true;
                addSongToList(songTitle, artist);

            }else if (stringExtra.contains("https://youtu.be")){ //from youtube

                quickCompletion = false;
                new GetYTInfo().execute(stringExtra.substring(stringExtra.indexOf("youtu.be/")+"youtu.be/".length()));

            } else {
                quickCompletion = true;
                complete(R.string.unkown_intent_rationale, false);
                success = false;
            }

            try {
                statsHelper.newStat(StatsHelper.ACTION_SHARE_RECEIVED, new JSONObject().put("intent_text", intent.getStringExtra(Intent.EXTRA_TEXT)).put("processed", success));
            } catch (JSONException ignored) {}

        }}else{
            quickCompletion = true;
            complete(R.string.unkown_intent_rationale, false);
        }

    }

    private void addSongToList(String songTitle, String artist) {

        switch (liedjiesHelper.addSongToList(songTitle, artist)){

            case EMPTY:
                complete(R.string.something_went_terribly_wrong, false);
                break;

            case ALREADY_EXISTS:
                complete(R.string.song_already_listed, true);
                break;

            case SUCCESS:
                complete(R.string.song_added, true);
                break;

        }

    }

    public void onOpenNabberClick(View view) {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    public void onDoneClick(View view) {
        finish();
    }

    private class GetGoogleInfo extends AsyncTask<String, Void, String[]> {

        protected String[] doInBackground(String... params) {

            String url = params[0];
            String songTitle = params[1];
            String artist;

            try{

                Document redirPage = Jsoup.connect(url).get();
                String newURL = "https://www.google.com" + redirPage.select("div [style=\"display:block\"]").get(0).select("a").get(0).attr("href");
                Document googlePage = Jsoup.connect(newURL).get();
                artist = googlePage.select("._Xbe").select(".kno-fv").get(0).select("a").get(0).html();

            }catch (IOException e){
                e.printStackTrace();
                complete(R.string.connection_issue, false);
                return null;
            }

            return new String[]{songTitle, artist};

        }

        protected void onPostExecute(String[] result) {

            if (result != null){
                addSongToList(result[0], result[1]);
            }

        }
    }

    private class GetYTInfo extends AsyncTask<String, Void, String[]>{

        @Override
        protected String[] doInBackground(String... videoID) {

            try {
                String embedUrl = "http://www.youtube.com/oembed?url=youtube.com/watch?v=" +
                        videoID[0] + "&format=json";

                return new String[]{
                        new JSONObject(Jsoup.connect(embedUrl).ignoreContentType(true).timeout(3_000).get()
                                .select("body").html()).getString("title"),
                        videoID[0]
                };

            }catch (IOException|JSONException e){
                e.printStackTrace();
                return null;
            }

        }

        @Override                       //title, videoID
        protected void onPostExecute(String[] result) {

            if (result == null){
                complete(R.string.youtube_title_fail, false);

            }else {

                addSongToList(result[0], "");

                ContentValues toInsert = new ContentValues();
                toInsert.put(LiedjiesContract.TblLinks.COLUMN_SONG_ID, liedjiesHelper.getSongID(result[0]));
                toInsert.put(LiedjiesContract.TblLinks.COLUMN_URL, "youtube.com/watch?v="+result[1]);

                liedjiesHelper.getDB().insert(
                        LiedjiesContract.TblLinks.TABLE_NAME,
                        null,
                        toInsert
                );
            }
        }
    }

    private void complete(@StringRes int msgID, boolean good){

        outcome = good;
        outcomeText = getString(msgID);

        if (quickCompletion){
            new WaitASecond().execute();
        }else {
            progressCircle.beginFinalAnimation();
        }

    }

    private class WaitASecond extends AsyncTask<Void, Void, Void>{
        @Override
        protected Void doInBackground(Void... voids) {
            try{
                Thread.sleep(1000);
            }catch (InterruptedException ignored){}
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            progressCircle.beginFinalAnimation();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        liedjiesHelper.close();
    }
}
