package com.sps.mus

import android.app.AlertDialog
import android.os.AsyncTask
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.CheckBox
import com.sps.mus.utils.JanTools
import com.sps.mus.utils.MyProgressDialog
import com.sps.mus.utils.RautenAPI
import com.sps.mus.utils.StatsHelper
import kotlinx.android.synthetic.main.activity_feedback.*
import org.json.JSONObject

class FeedbackActivity : AppCompatActivity() {

    private var jantools: JanTools? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_feedback)

        setSupportActionBar(findViewById(R.id.toolbar))
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        jantools = JanTools(this)

        val email: String? = jantools!!.readPref(JanTools.PREF_FEEDBACK_EMAIL)

        if ( email != null){
            edtFeedbackEmail.setText(email)
        }

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        if (item?.itemId == android.R.id.home) {
            finish()
            return true

        }else if (item?.itemId == R.id.menu_item_send_feedback){

            StatsHelper(this).uploadStats()

            when {
                edtFeedback.text.toString().length < 4 ->
                    jantools?.makeToast(getString(R.string.feedback_empty))
                edtFeedbackEmail.text.toString().length <= 5 && !chkAnonymous.isChecked->
                    jantools?.makeToast(getString(R.string.feedback_email_empty))
                edtFeedback.text.toString().length > 65_535 ->
                    jantools?.makeToast(getString(R.string.feedback_too_long))
                edtFeedbackEmail.text.toString().length > 255 ->
                    jantools?.makeToast(getString(R.string.feedback_email_too_long))

                else -> {
                    SendFeedback().execute()

                }

            }

            return true

        }else {
            return super.onOptionsItemSelected(item)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_feedback, menu)

        return true
    }

    private inner class SendFeedback : AsyncTask<Void, Void, Boolean>(){

        private var dialog : AlertDialog? = null

        override fun onPreExecute() {
            dialog = MyProgressDialog(this@FeedbackActivity)
                    .setMessage(R.string.loading_sending_feedback)
                    .build()
            dialog!!.show()
        }

        override fun doInBackground(vararg v: Void?): Boolean {

            val jsonToSend = JSONObject()
            jsonToSend.put("GUID", jantools!!.guid)
                    .put("versionCode", jantools!!.versionCode)
                    .put("feedbackText", edtFeedback.text.toString())

            if (chkAnonymous.isChecked)
                jsonToSend.put("email", "Anonymous")
            else {
                jsonToSend.put("email", edtFeedbackEmail.text.toString())
                jantools!!.writePref(JanTools.PREF_FEEDBACK_EMAIL, edtFeedbackEmail.text.toString())
            }

            val jsonReceived : JSONObject = RautenAPI(RautenAPI.URL_FEEDBACK).makeReq(jsonToSend, false)

            return jsonReceived.getBoolean("success")

        }

        override fun onPostExecute(result: Boolean?) {
            dialog!!.dismiss()

            if (result == true){

                AlertDialog.Builder(this@FeedbackActivity)
                        .setMessage(getString(R.string.msg_feedback_sent))
                        .setNeutralButton(android.R.string.ok, { _, _ ->

                            jantools?.hideKeyboard()
                            finish()

                        })
                        .setCancelable(false)
                        .show()

            }else{
                jantools?.makeToast(getString(R.string.feedback_failed))
            }

        }

    }

    fun onAnonClick(chk: View){

        if (chkAnonymous.isChecked){

            edtFeedbackEmail.setText(R.string.chk_anonymous)
            edtFeedbackEmail.isEnabled = false

        }else{

            edtFeedbackEmail.setText("")
            edtFeedbackEmail.isEnabled = true

        }

    }

}
