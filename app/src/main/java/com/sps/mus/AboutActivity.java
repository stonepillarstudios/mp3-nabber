package com.sps.mus;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.sps.mus.utils.JanTools;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class AboutActivity extends Activity {

    private final static String APP_COPYRIGHT_URL = "https://sps.rauten.co.za/mp3-nabber/copyright-policy";
    private final static String SHARE_LINK = "https://sps.rauten.co.za/mp3-nabber";

    private final static String BITCOIN_ADDRESS = "1B6bdPiAqCCzqJAWgj6vk5jmStSg5tUW6e";
    private final static String ETHEREUM_ADDRESS = "0xD9Dea0CEF8852b136282C889C5c83871ad32254D";

    private JanTools jantools;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        jantools = new JanTools(this);

        ((TextView) findViewById(R.id.txtAppName)).setText(
                String.format("%s %s", getString(R.string.app_name), jantools.getVersionName())
        );

        ((TextView) findViewById(R.id.txtCopyright)).setText(
                String.format("© %s %s", new SimpleDateFormat("yyyy", Locale.ENGLISH).format(new Date()), getString(R.string.company_name))
        );

        ((TextView) findViewById(R.id.txtTotalDownloaded)).setText(
                String.format(getString(R.string.download_statistics), Integer.valueOf(jantools.readPref(JanTools.PREF_TOTAL_DOWNLOADS)))
        );

        TextView txtReadMore = findViewById(R.id.txtReadMore);
        txtReadMore.setText(
                Html.fromHtml(String.format("<a href = \"%s\">%s</a>", APP_COPYRIGHT_URL, getString(R.string.read_more)))
        );
        txtReadMore.setMovementMethod(LinkMovementMethod.getInstance());

        TextView donateBitcoin = findViewById(R.id.donateBitcoin);
        donateBitcoin.setText(
                Html.fromHtml(String.format("<a href=\"bitcoin:%s?label=Stone%%20Pillar%%20Studios\">%s</a>",
                        BITCOIN_ADDRESS, BITCOIN_ADDRESS))
        );
        donateBitcoin.setMovementMethod(LinkMovementMethod.getInstance());

        TextView donateEthereum = findViewById(R.id.donateEthereum);
        donateEthereum.setText(
                Html.fromHtml(String.format("<a href=\"ethereum:%s?label=Stone%%20Pillar%%20Studios\">%s</a>",
                        ETHEREUM_ADDRESS, ETHEREUM_ADDRESS))
        );
        donateEthereum.setMovementMethod(LinkMovementMethod.getInstance());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home){
            finish();
            return true;
        }else return false;

    }

    public void onBitcoinClick(View view) {

        try {
            ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData clip = ClipData.newPlainText(getString(R.string.bitcoin_copy_label), BITCOIN_ADDRESS);
            clipboard.setPrimaryClip(clip);
            jantools.makeToast(getString(R.string.address_copied));
        }catch (NullPointerException e){
            e.printStackTrace();
            jantools.makeToast(getString(R.string.copy_clipboard_failed));
        }

    }

    public void onEthereumClick(View view) {

        try {
            ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData clip = ClipData.newPlainText(getString(R.string.ethereum_copy_label), ETHEREUM_ADDRESS);
            clipboard.setPrimaryClip(clip);
            jantools.makeToast(getString(R.string.address_copied));
        }catch (NullPointerException e){
            e.printStackTrace();
            jantools.makeToast(getString(R.string.copy_clipboard_failed));
        }

    }

    public void onShareClick(View view) {
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.putExtra(Intent.EXTRA_TEXT,
                String.format("%s: %s", getString(R.string.catchphrase), SHARE_LINK));
        shareIntent.setType("text/plain");
        startActivity(Intent.createChooser(shareIntent,
                String.format(getString(R.string.share_via), getString(R.string.app_name))
        ));
    }
}
