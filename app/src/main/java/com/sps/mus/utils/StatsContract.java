package com.sps.mus.utils;

import android.provider.BaseColumns;

import com.google.common.collect.Table;

/**
 * Created by koell on 05 Jan 2018.
 */

public final class StatsContract {

    private StatsContract(){}

    public static class TableDetails implements BaseColumns{
        public static final String TABLE_NAME = "tblLocalStats";
        public static final String COLUMN_TIMESTAMP = "action_timestamp";
        public static final String COLUMN_ACTION = "action";
        public static final String COLUMN_DETAILS = "details";
        public static final String COLUMN_VERSION = "version";
    }

    public static final String SQL_CREATE_TABLE =
            "CREATE TABLE IF NOT EXISTS " + TableDetails.TABLE_NAME +
                    " (" + TableDetails._ID + " INTEGER PRIMARY KEY, " +
                    TableDetails.COLUMN_TIMESTAMP + " LONG, " +
                    TableDetails.COLUMN_ACTION + " TEXT, " +
                    TableDetails.COLUMN_DETAILS + " TEXT )";

}
