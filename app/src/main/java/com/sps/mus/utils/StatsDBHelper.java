package com.sps.mus.utils;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by koell on 05 Jan 2018.
 */

public class StatsDBHelper extends SQLiteOpenHelper {

    public static final int DB_VERSION = 2;     //inc at upgrade
    public static final String DB_NAME = "Stats.db";

    public StatsDBHelper(Context context){
        super(context, DB_NAME, null, DB_VERSION);
    }

    public void onCreate(SQLiteDatabase db){
        db.execSQL(StatsContract.SQL_CREATE_TABLE);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //policy: del all and begin again. Hopefully db layout never changes

        if (oldVersion == 1 && newVersion == 2) {
            db.execSQL("ALTER TABLE " + StatsContract.TableDetails.TABLE_NAME +
                    " ADD " + StatsContract.TableDetails.COLUMN_VERSION + " INT DEFAULT 0");
        }else {
            onCreate(db);
        }
    }

}
