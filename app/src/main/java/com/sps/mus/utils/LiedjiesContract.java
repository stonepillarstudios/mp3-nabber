package com.sps.mus.utils;

import android.provider.BaseColumns;

import org.jsoup.Connection;

/**
 * Created by Jans Rautenbach on 08 Jul 2017.
 */

public final class LiedjiesContract {

    private LiedjiesContract(){}

    public static class TblLiedjies implements BaseColumns{
        public static final String TABLE_NAME = "tblLiedjies";
        public static final String COLUMN_SONG = "LiedjieNaam";
        public static final String COLUMN_ARTIST = "Kunstenaar";
        public static final String COLUMN_DATE_ADDED = "DatumByGevoeg";
    }

    public static class TblLinks implements BaseColumns{
        public static final String TABLE_NAME = "tblLinks";
        public static final String COLUMN_URL = "URL";
        public static final String COLUMN_SONG_ID = "songID";
    }

    public static class TblArtists implements BaseColumns{
        public static final String TABLE_NAME = "tblArtists";
        public static final String COLUMN_ARTIST = "artist";
    }

    public static final String SQL_CREATE_TblLiedjies =
            "CREATE TABLE IF NOT EXISTS " + TblLiedjies.TABLE_NAME +
                " (" + TblLiedjies._ID + " INTEGER PRIMARY KEY, " +
                TblLiedjies.COLUMN_SONG + " TEXT, " +
                TblLiedjies.COLUMN_ARTIST + " TEXT, " +
                TblLiedjies.COLUMN_DATE_ADDED + " TEXT)";  //yyyy-MM-dd HH:mm:ss

    public static final String SQL_CREATE_TblLinks =
            "CREATE TABLE IF NOT EXISTS " + TblLinks.TABLE_NAME +
                    " ("+TblLinks._ID + " INETEGER PRIMARY KEY, " +
                    TblLinks.COLUMN_SONG_ID + " INTEGER, " +
                    TblLinks.COLUMN_URL + " TEXT)";

    public static final String SQL_CREATE_TblArtists =
            "CREATE TABLE IF NOT EXISTS " + TblArtists.TABLE_NAME +
                    " ("+TblArtists._ID + " INTEGER PRIMARY KEY, " +
                    TblArtists.COLUMN_ARTIST + " TEXT)";

}
