package com.sps.mus;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.ToggleButton;
import com.sps.mus.utils.JanTools;
import com.sps.mus.utils.LiedjiesContract;
import com.sps.mus.utils.LiedjiesDBHelper;
import com.sps.mus.utils.StatsHelper;
import org.json.JSONException;
import org.json.JSONObject;

public class SettingsActivity extends AppCompatActivity {

    private JanTools jantools;

    public static final String KEY_DOWNLOAD_ON_MOBILE = "download_on_mobile";
    public static final String KEY_SERVER_ERROR = "server_error_action";
    public static final String KEY_CONNECTION_ISSUE = "connection_issue_action";
    public static final String KEY_FAILS_TO_STOP = "fails_before_stopping";

    private ToggleButton tglMobile;
    private Spinner spinServerErrorAction;
    private Spinner spinConnectionIssueAction;
    private Spinner spinQuality;
    private EditText edtFailsToStop;

    private JSONObject settingsObj;
    private boolean userSpinAction = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        setSupportActionBar(((Toolbar) findViewById(R.id.toolbar)));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        jantools = new JanTools(this);

        settingsObj = new JSONObject();

        tglMobile = ((ToggleButton) findViewById(R.id.tglMobile));
        spinServerErrorAction = ((Spinner) findViewById(R.id.spinServerErrorAction));
        spinConnectionIssueAction = ((Spinner) findViewById(R.id.spinConnectionIssueAction));
        spinQuality = findViewById(R.id.spinQuality);
        edtFailsToStop = findViewById(R.id.edtFailsToStop);

        if (jantools.readPrefBool(KEY_DOWNLOAD_ON_MOBILE))
            tglMobile.setChecked(true);

        edtFailsToStop.setText(jantools.readPref(KEY_FAILS_TO_STOP));
        edtFailsToStop.addTextChangedListener(failsTextWatcher);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item);
        adapter.addAll(getResources().getStringArray(R.array.pref_postfailactions));

        spinServerErrorAction.setAdapter(adapter);
        spinConnectionIssueAction.setAdapter(adapter);

        spinServerErrorAction.setSelection(Integer.valueOf(jantools.readPref(KEY_SERVER_ERROR)));
        spinConnectionIssueAction.setSelection(Integer.valueOf(jantools.readPref(KEY_CONNECTION_ISSUE)));

        AdapterView.OnItemSelectedListener spinItemSelListener = new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (userSpinAction) {
                    switch (parent.getId()) {
                        case R.id.spinServerErrorAction:
                            jantools.writePref(KEY_SERVER_ERROR, position+"");
                            try {
                                settingsObj.put("server_error_action", position);
                            } catch (JSONException ignored) {
                            }

                            break;

                        case R.id.spinConnectionIssueAction:
                            jantools.writePref(KEY_CONNECTION_ISSUE, position+"");
                            try {
                                settingsObj.put("connection_issue_action", position);
                            } catch (JSONException ignored) {
                            }

                            break;
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                //do nothing
            }
        };

        spinConnectionIssueAction.setOnItemSelectedListener(spinItemSelListener);
        spinServerErrorAction.setOnItemSelectedListener(spinItemSelListener);


        ArrayAdapter<String> qualityAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item);
        qualityAdapter.addAll(getResources().getStringArray(R.array.qualities));

        spinQuality.setAdapter(qualityAdapter);

        spinQuality.setSelection(Integer.valueOf(jantools.readPref(JanTools.PREF_QUALITY)));

        spinQuality.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long l) {
                if (userSpinAction) {
                    jantools.writePref(JanTools.PREF_QUALITY, pos + "");
                    try {
                        settingsObj.put("quality", pos);
                    } catch (JSONException ignored) {
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                //do nothing
            }
        });

    }

    private TextWatcher failsTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

        @Override
        public void onTextChanged(CharSequence s, int i, int i1, int i2) {
            if (s.length() > 0){
                if (s.toString().equals("0")){
                    jantools.makeToast(getString(R.string.fails_no_0), true);
                    edtFailsToStop.setText("");
                }else {
                    jantools.writePref(KEY_FAILS_TO_STOP, s.toString());
                    try {
                        settingsObj.put("fails_to_stop", Integer.valueOf(s+""));
                    } catch (JSONException ignored) {}
                }
            }
        }

        @Override
        public void afterTextChanged(Editable editable) {}
    };

    public void onTglMobileClick(View v){

        try {
            settingsObj.put("mobile_data", tglMobile.isChecked());
        } catch (JSONException ignored) {}

        jantools.writePref(KEY_DOWNLOAD_ON_MOBILE, tglMobile.isChecked() + "");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home){
            finish();
        }

        return true;

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (isFinishing()){
            if (settingsObj.length() > 0) {
                new StatsHelper(this).newStat(StatsHelper.ACTION_SETTINGS_CHANGE, settingsObj);
                System.out.println("settingsObj: " + settingsObj.toString());
            }
        }
    }

    public void onResetClick(View view) {

        new AlertDialog.Builder(this)
                .setMessage(R.string.reset_info)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        jantools.resetPrefs();
                        jantools.makeToast(getString(R.string.reset_done));

                        try {
                            new StatsHelper(SettingsActivity.this).newStat(StatsHelper.ACTION_SETTINGS_RESET, new JSONObject()
                                    .put("subject", "shared_preferences"));
                        }catch(JSONException ignored){}

                        setResult(RESULT_OK);
                        finish();
                    }
                })
                .setNegativeButton(R.string.button_nevermind, null)
                .show();

    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        userSpinAction = true;
    }

    public void onClearDBClick(View view) {

        new AlertDialog.Builder(this)
                .setMessage(R.string.clear_downloads_info)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        SQLiteDatabase db = new LiedjiesDBHelper(SettingsActivity.this).getWritableDatabase();
                        db.delete(LiedjiesContract.TblLiedjies.TABLE_NAME, null, null);
                        db.delete(LiedjiesContract.TblLinks.TABLE_NAME, null, null);

                        try {
                            new StatsHelper(SettingsActivity.this).newStat(StatsHelper.ACTION_SETTINGS_RESET, new JSONObject()
                                    .put("subject", "databases"));
                        }catch(JSONException ignored){}

                        setResult(RESULT_OK);
                        finish();
                    }
                })
                .setNegativeButton(R.string.button_nevermind, null)
                .show();

    }
}

